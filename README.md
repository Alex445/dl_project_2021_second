# DL_project_2021_second

[program](https://docs.google.com/document/d/1MzdbmrhabHUIb5-I-WGi_PSe_8J25_utfKjmfKUtfAc/edit)

## Task 
Задача: Topic Modeling

[Датасет](https://www.kaggle.com/datatattle/covid-19-nlp-text-classification)

## Members
1. **Ярослав Русин** - team lead
2. Екатерина Пуговкина - developer
3. Фида Александр - developer

## Workspace
> [trello](https://trello.com/b/lXawEUG9/dlproject)


## Description

### Цель: 
реализовать кластеризацию набора текстов.
Для кластеризации текстов использовалась нейронная тематическая модель для автоматического извлечения тем из текста, т.к. она позволяет избежать сложных математических выводов для вывода модели. Однако большинство моделей на данный момент базируются на неправильном априорном распределении (например, распределение Гаусса) над скрытым тематическим пространством. Поэтому мы использовали модель BAT (Bidirectional Adversarial Topic), которая представляет собой попытку применения двунаправленного состязательного обучения для нейронного тематического моделирования.

### Результаты:
(как главная метрика рассматривается c_v)
c_v = 0.63. Эта метрика отражает когерентность тематических топиков. В нашем случае она составляет 0,63, что является неплохим результатом. Оптимальный диапазон для метрики 0.4 до 0.9.


Получившиеся тематические кластеры:


['funny', 'auto', 'ordered', 'attend', 'winter', 'return', 'coronapocolypse', 'mo', 'fb', 'town', 'exhibit', 'poor', 'interesting', 'registered', 'iraq']
['damn', 'funny', 'getting', 'got', 'coronapocolypse', 'bh', 'hall', 'install', 'someone', 'regulated', 'town', 'environment', 'military', 'thing', 'interesting']
['greater', 'extra', 'funny', 'tender', 'coronapocolypse', 'town', 'avoiding', 'waste', 'auto', 'environment', 'particularly', 'ordered', 'serve', 'poor', 'film']
['funny', 'seller', 'coronapocolypse', 'ordered', 'town', 'poor', 'causing', 'interesting', 'sanitizing', 'pray', 'mo', 'mu', 'evidence', 'meter', 'wo']
['think', 'advantage', 'funny', 'coronapocolypse', 'town', 'interesting', 'waste', 'saving', 'al', 'david', 'film', 'auto', 'jx', 'ordered', 'poor']
['t', 'roll', 'endure', 'funny', 'won', 'changing', 'coronapocolypse', 'town', 'interesting', 'poor', 'exhibit', 'saving', 'avoiding', 'grocerystores', 'ordered']
['real', 'funny', 'whole', 'donald', 'town', 'coronapocolypse', 'rember', 'doe', 'ordered', 'tweet', 'causing', 'poor', 'environment', 'party', 'return']
['funny', 'auto', 'winter', 'won', 'ring', 'attend', 'town', 'figured', 'pa', 'coronapocolypse', 'league', 'session', 'recovered', 'registered', 'host']
['staff', 'driver', 'coronapocolypse', 'funny', 'town', 'nh', 'return', 'palm', 'tuned', 'avoiding', 'producing', 'poor', 'meter', 'pa', 'mar']
['donation', 'bottle', 'funny', 'interesting', 'coronapocolypse', 'town', 'ordered', 'tweet', 'mu', 'retweet', 'sanitizing', 'en', 'auto', 'coronavirusmes', 'meter']
['product', 'higher', 'funny', 'india', 'town', 'en', 'coronapocolypse', 'causing', 'auto', 'thousand', 'interesting', 'al', 'ordered', 'meter', 'saving']
['h', 'distancing', 'funny', 'return', 'coronapocolypse', 'town', 'ordered', 'poor', 'grocerystores', 'mo', 'film', 'saving', 'coronavirusmes', 'meter', 'party']
['man', 'everyone', 'funny', 'coronapocolypse', 'said', 'return', 'saving', 'town', 'environment', 'grocerystores', 'poor', 'coronavirusmes', 'pray', 'al', 'ordered']
['funny', 'town', 'attend', 'coronapocolypse', 'auto', 'won', 'film', 'ring', 'saving', 'smell', 'registered', 'recovered', 'return', 'exhibit', 'shameful']
['funny', 'coronapocolypse', 'town', 'environment', 'ordered', 'install', 'tweet', 'socially', 'indoors', 'switzerland', 'regulated', 'interesting', 'gang', 'initiative', 'braved']

### Результат тренировки модели:


![Model training result](resources/batm_trainloss.png)
