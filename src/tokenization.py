import re
from typing import List
from collections import defaultdict
import multiprocessing
from tqdm import tqdm
from pyhanlp import *
import spacy
import pandas as pd
from cleaning import preprocess_word

LANG_CLS = defaultdict(lambda: "Tokenizer")


class Tokenizer(object):
    def __init__(self, lang="en", stopwords=None):
        self.stopwords = stopwords
        print("Tokenization ...")

    def tokenize(self, df: pd.DataFrame, column_name: str) -> List[List[str]]:
        docs = df[column_name].apply(lambda row: preprocess_word(row))
        return docs.to_list()


if __name__ == '__main__':
    tokenizer = Tokenizer()
    print(tokenizer.tokenize(['tokenization....']))